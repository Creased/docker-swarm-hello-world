var app    = require('express')(),               // Routes
    server = require('http').createServer(app),  // HTTP server
    redis  = require('redis'),                   // Redis
    os     = require('os');                      // OS commands

var config = {
    'redis': {  // Redis server
        'host': process.env.REDIS_HOST || '127.0.0.1',
        'port': process.env.REDIS_PORT || 6379
    },
    'app': {  // Listening host and port
        'host': process.env.APP_HOST || '0.0.0.0',
        'port': process.env.APP_PORT || 8080
    }
};

var client = redis.createClient(config.redis.port, config.redis.host);

// Index
app.get('/', function (req, res, next) {
    client.incr('views', function (err, views) {
        if (err) {
            console.log("Error");
            return next(err);
        }

        res.render('index.ejs', {
            'views': views,
            'hostname': os.hostname()
        });
    });
});

// Start server
server.listen(config.app.port, config.app.host);
